﻿using INITS.Core;
using Microsoft.EntityFrameworkCore;

namespace INITS.Data.Sql
{
    public class INITSDbContext : DbContext
    {
        public INITSDbContext(DbContextOptions<INITSDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            var modelExpense = builder.Entity<Expense>().ToTable("Expense");

            modelExpense.HasKey(x => x.Id);
        }

        internal DbSet<Expense> Expenses { get; set; }
    }
}
