﻿using INITS.Core.Interface.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

namespace INITS.Data.Sql.Generics
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private readonly INITSDbContext _dbContext;
        private DbSet<T> table = null;

        public GenericRepository(INITSDbContext dbContext)
        {
            _dbContext = dbContext;
            table = _dbContext.Set<T>();
        }
        public void Create(T model)
        {
            table.Add(model);

            _dbContext.SaveChanges();
        }

        public void Delete(params object[] ids)
        {
            var items = table.Find(ids);

            table.RemoveRange(items);

            _dbContext.SaveChanges();
        }

        public async Task<IList<T>> GetAllAsync()
        {
            return await table.ToListAsync();
        }

        public T GetById(object id)
        {
            return table.Find(id);
        }

        public void Update(T model)
        {
            table.Attach(model);

            _dbContext.Entry(model).State = EntityState.Modified;

            _dbContext.SaveChanges();
        }
    }
}
