# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository is setup to profile a INITS Test Project for savinf Expense

### How do I get set up? ###

First of all, create a database name of "INITSDb" and then connect to it using the appSettings.json database connection
string within the file.

The database part of the project is ran against the newly created INITSDb database to get the changes that is needed
using SCHEMA COMPARE on the DATABASE PART OF THE PROJECT.

Then set the APIService as the default project and then run the project based on the VS settings provided to 
get the browser to which the application is to be loaded.

At the running of the project, SWAGGER UI is automatically loaded on the browser where different endpoint 
avaialble within the system can be seen and tested based on the stories discussed.

### Contribution guidelines ###
There are a couple of libraries that were introduced into the project and a few of them is AutoMapper, 
Entity Framework as an ORM,and others.

### How do you build the project? ###

Using Onion Architecture to setup the project, I added some libraries to help enhance my project.
At the core section of the project I created Entity Expense which will be used to save the model in the database.
Once that was created, i created interfaces which includes the IGenericRepository which was 
implemented in the Data.Sql in the Domain Folder where the DbContext for the system was set up and also the and 
IExpenseService implemented at the Infrastructure Folder.

The DATABASE table of Expense is created and then setup inside the Database Folder in a MSSQL

After all that, the Api Endpoint was set up at the APIService Folder where all the created services setup in 
the ExpenseService was implemented to create required endpoint as needed.

### How to use the API Created based on the Story? ###
Story 1:

Call Api Endpoint -- "~/api/Expense/create" to save the a new Expense End point

Story 2:
Call Api Enpoint -- "~/api/Expense" to get all the saved Expenses in the system

In addition:

There are other Endpoints created in the system since they would be required for the CRUD operations.