﻿using Microsoft.Extensions.Configuration;
using System;

namespace INITS.Core.Configurations
{
    public static class IocContainerConfig
    {
        public static IServiceProvider Provider { get; set; }

        public static IConfiguration Configuration { get; set; }
    }
}
