﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace INITS.Core.Helpers
{
    public static class ObjectExtensions
    {
        public static bool IsNull<T>(this T source)
        {
            return source == null;
        }

        public static bool IsNullOrEmpty<T>(this IEnumerable<T> source)
        {
            return source == null || source.Count() == default;
        }
    }
}
