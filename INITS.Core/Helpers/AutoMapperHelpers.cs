﻿using System;
using System.Collections.Generic;
using System.Text;

namespace INITS.Core.Helpers
{
    public static class AutoMapperHelpers
    {
        public static bool CheckDefaultValue(this object source)
        {
            return source == default;
        }

        public static decimal CalculateVAT(this Expense source)
        {
            return (75 * source.Value) / 1000;
        }
    }
}
