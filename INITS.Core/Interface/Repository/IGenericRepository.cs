﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace INITS.Core.Interface.Repository
{
    public interface IGenericRepository<T> where T : class
    {
        void Create(T model);

        void Delete(params object[] ids);

        void Update(T model);

        Task<IList<T>> GetAllAsync();

        T GetById(object id);
    }
}
