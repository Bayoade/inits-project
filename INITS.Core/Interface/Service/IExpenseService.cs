﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace INITS.Core.Interface.Service
{
    public interface IExpenseService
    {
        Guid CreateExpense(Expense model);

        bool DeleteExpense(params Guid[] ids);

        bool UpdateExpense(Expense model);

        Expense GetExpenseById(Guid id);

        Task<IList<Expense>> GetAllExpenses();
    }
}
