﻿using System;

namespace INITS.Core
{
    public class Expense
    {
        public Guid Id { get; set; }
        public DateTime ExpenseDate { get; set; }
        public decimal Value { get; set; }
        public string Reason { get; set; }
        public bool IsDeleted { get; set; }
    }
}
