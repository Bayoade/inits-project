﻿using AutoMapper;
using INITS.Core;
using INITSProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using INITS.Core.Helpers;

namespace INITSProject
{
    public class APIServiceProfileMapper : Profile
    {
        public APIServiceProfileMapper()
        {
            CreateMap<Expense, ExpenseViewModel>()
                .ForMember(x => x.ValueVAT, y => y.MapFrom(z => z.CalculateVAT()))
                .ReverseMap();

            CreateMap<Expense, ExpenseDetails>()
                .ForMember(x => x.ValueVAT, y => y.MapFrom(z => z.CalculateVAT()))
                .ReverseMap();

            CreateMap<Expense, SaveExpenseViewModel>()
               .ReverseMap();
        }
    }
}   
