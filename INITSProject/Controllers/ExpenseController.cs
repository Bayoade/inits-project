﻿using AutoMapper;
using INITS.Core;
using INITS.Core.Helpers;
using INITS.Core.Interface.Service;
using INITSProject.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace INITSProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExpenseController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IExpenseService _expenseService;

        /// <summary>
        /// constructor to initialize dependencies
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="expenseService"></param>
        public ExpenseController(IMapper mapper, IExpenseService expenseService)
        {
            _mapper = mapper;
            _expenseService = expenseService;
        }

        /// <summary>
        /// Gets all the Expense in the system
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAllExpense()
        {
            var expenses = await _expenseService.GetAllExpenses();

            if (expenses.IsNullOrEmpty())
            {
                return Ok(expenses);
            }

            return Ok(_mapper.Map<List<ExpenseViewModel>>(expenses));
        }

        /// <summary>
        /// Create Expense
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("create")]
        [HttpPost]
        public IActionResult CreateExpense([FromBody] SaveExpenseViewModel model)
        {
            return Ok(_expenseService.CreateExpense(_mapper.Map<Expense>(model)));
        }

        /// <summary>
        /// Updates the expense
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("update")]
        [HttpPut]
        public IActionResult UpdateExpense([FromBody] SaveExpenseViewModel model)
        {
            var expense = _mapper.Map<Expense>(model);
            var result = _expenseService.UpdateExpense(expense);

            if (!result)
            {
                return Ok(new ErrorMessage
                {
                    Message = $"Expense { model.Reason } failed to delete",
                    Success = false
                });
            }
            return Ok(new ErrorMessage
            {
                Message = $"Successful updated Expense {model.Reason}",
                Success = true
            });
        }

        /// <summary>
        /// Deletes expense by the id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("delete/{id:Guid}")]
        [HttpDelete]
        public IActionResult DeleteExpense(Guid id)
        {
            var expense = _expenseService.GetExpenseById(id);

            if (expense.IsNull())
            {
                return Ok(new ErrorMessage
                {
                    Message = "Expense is unavailable",
                    Success = false
                });
            }

            expense.IsDeleted = true;

            var result = _expenseService.UpdateExpense(expense);

            if (!result)
            {
                return Ok(new ErrorMessage
                {
                    Message = $"Expense {expense.Reason} failed to delete",
                    Success = false
                });
            }
            
            return Ok(new ErrorMessage
            {
                Message = $"Successful deleted Expense {expense.Reason}",
                Success = true
            });
        }

        /// <summary>
        /// Gets the Expense by id with all details with VAT
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("{id:Guid}/details")]
        [HttpGet]
        public IActionResult GetExpenseById(Guid id)
        {
            var result = _expenseService.GetExpenseById(id);

            if (result.IsNull())
            {
                return Ok(result);
            }

            return Ok(_mapper.Map<ExpenseDetails>(result));
        }

        /// <summary>
        /// Gets the Expense by id without VAT
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("{id:Guid}")]
        [HttpGet]
        public IActionResult GetExpenseByIdWithoutVAT(Guid id)
        {
            var result = _expenseService.GetExpenseById(id);

            if (result.IsNull())
            {
                return Ok(result);
            }

            return Ok(_mapper.Map<SaveExpenseViewModel>(result));
        }
    }
}