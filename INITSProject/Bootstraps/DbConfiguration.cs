﻿using INITS.Core.Configurations;
using INITS.Data.Sql;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace INITSProject.Bootstraps
{
    public static class DbConfiguration
    {
        public static void AddDbConfiguration(this IServiceCollection services)
        {
            services.AddDbContext<INITSDbContext>(config => config.UseSqlServer(IocContainerConfig.Configuration.GetConnectionString("INITSDbConnectionKey")));
        }
    }
}
