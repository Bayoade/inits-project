﻿namespace INITSProject.ViewModel
{
    public class ErrorMessage
    {
        public string Message { get; set; }

        public bool Success { get; set; }
    }
}
