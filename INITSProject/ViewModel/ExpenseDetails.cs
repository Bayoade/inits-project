﻿using System;

namespace INITSProject.ViewModel
{
    public class ExpenseDetails
    {
        public Guid Id { get; set; }
        public DateTime ExpenseDate { get; set; }
        public string Reason { get; set; }
        public double Value { get; set; }
        public double ValueVAT { get; set; }
    }
}
