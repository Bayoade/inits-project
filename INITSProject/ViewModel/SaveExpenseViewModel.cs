﻿using System;
using System.ComponentModel.DataAnnotations;

namespace INITSProject.ViewModel
{
    public class SaveExpenseViewModel
    {
        public Guid Id { get; set; }

        [Required]
        public DateTime ExpenseDate { get; set; }

        [Required]
        public string Reason { get; set; }

        [Required]
        public double Value { get; set; }
    }
}
