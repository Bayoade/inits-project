﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace INITSProject.ViewModel
{
    public class ExpenseViewModel
    {
        public Guid Id { get; set; }
        public DateTime ExpenseDate { get; set; }
        public string Reason { get; set; }
        public double ValueVAT { get; set; }
    }
}
