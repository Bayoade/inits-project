﻿using INITS.Core;
using INITS.Core.Interface.Repository;
using INITS.Core.Interface.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace INITS.Infrastructure.Services
{
    public class ExpenseService : IExpenseService
    {
        public IGenericRepository<Expense> _expenseRepository;

        public ExpenseService(IGenericRepository<Expense> expenseRepository)
        {
            _expenseRepository = expenseRepository;
        }
        public Guid CreateExpense(Expense model)
        {
            model.Id = Guid.NewGuid();

            _expenseRepository.Create(model);

            return model.Id;
        }

        public bool DeleteExpense(params Guid[] ids)
        {
            _expenseRepository.Delete(ids);

            return true;
        }

        public async Task<IList<Expense>> GetAllExpenses()
        {
            var allExpense = await _expenseRepository.GetAllAsync();

            return allExpense.Where(x => !x.IsDeleted).ToList();
        }

        public Expense GetExpenseById(Guid id)
        {
            var result = _expenseRepository.GetById(id);

            if(result.IsDeleted)
            {
                return null;
            }

            return result;
        }

        public bool UpdateExpense(Expense model)
        {
            try
            {
                _expenseRepository.Update(model);

                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }
    }
}
